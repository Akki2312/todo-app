import axios from "axios";

export const saveTodo = (todo) =>
  axios.post("https://todotestct.netlify.app/api/todos", todo);

export const loadTodos = () =>
  axios.get("https://todotestct.netlify.app/api/todos");

export const destroyTodo = (id) =>
  axios.delete(`https://todotestct.netlify.app/api/todos/${id}`);

export const updateTodo = (todo) =>
  axios.put(`https://todotestct.netlify.app/api/todos/${todo.id}`, todo);
